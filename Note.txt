* Module
- Cấp cao nhất trong ứng dụng, dùng để đóng gói 1 chức năng cụ thể trong ứng dụng
- 1 Module có thể chứa nhiều module hoặc component
- Có nhiều loại module: Do dev tự định nghĩa, các module được angular định nghĩa sẵn

* Component
- Dùng để biểu diễn UI và logic
- Một component bao gồm: HTML, CSS, Selector (tên thẻ component để gắn vào html), class để xử lý logic

** Mối quan hệ giữa module và component: module giống như 1 group của
component quản lý các component. 1 Module có thể quản lý nhiều component và mỗi
component phải được quản lý bởi module nào đó

Khởi chạy ứng dụng: ng serve --open

Lệnh tạo Component => ng g c <Tên Component>
Lệnh tạo Module => ng g m <Tên Module>
Lệnh tạo Component trong Module => ng g c TenComponent --module=<Tên folder Module>

=== Buổi 2 - 3 ===

* Data binding
	- Oneway Binding
		+ Interpolation - Sử dụng cặp dấu {{}} - Ex: {{name}}
		+ Property Binding - Sử dụng cặp dấu [] - Ex: [value]="name"
		+ Event Binding - Sử dụng cặp dấu () - Ex: (click)="tenHam()"
	- Twoway Binding
    + Sự kết hợp giữa property & event Binding [()] - Ex: [(ngModel)]="name"
    + Bắt buộc phải import FormsModule mới có thế sử dụng twoway-binding


* Directives
	- Structural Directive
		+ ngIf else
		+ ngSwitch
		+ ngFor

	- Attribute Directive
		+ ngClass
		+ ngStyle - Ít trường hợp dùng, dùng khi set background-image mà hình ảnh là dữ liệu động
		+ tự định nghĩa directive - ng g directive "ten directive"

* Input
	- Dùng để truyền dữ liệu từ component cha qua component con, component con khai báo Input

* Output
	- Dùng để truyền dữ liệu từ component con ra component cha, thông qua sự kiện (click, change,...) đại diện bởi đối tượng EventEmiter
	- Khai báo: @Output() eventName = new EventEmiter()
	- Sử dụng: this.eventName.emit(data)

* ViewChild
	- Dùng để DOM tới component hoặc thể trong thành phần html của component đóng
	- Khi đó ở class cha có thể sử dụng được các phương thức và thuộc tính của class con

* ViewChildren
	- Tương tự ViewChild nhưng DOM đến danh sách các thể hoặc component con

=== Buổi 4 ===
Routing Component
Routing Module

Lệnh tạo service: ng g service <TenService>














