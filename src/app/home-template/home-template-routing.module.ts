import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./contact/contact.component";
import { DetailComponent } from "./detail/detail.component";
import { HomeTemplateComponent } from "./home-template.component";
import { Baitap1Component } from '../baitap1/baitap1.component';
import { Baitap2Component } from '../baitap2/baitap2.component';
import { DataBindingComponent } from '../data-binding/data-binding.component';
import { AttributeDirectivesComponent } from '../attribute-directives/attribute-directives.component';
import { BaiTapDatGheComponent } from '../bai-tap-dat-ghe/bai-tap-dat-ghe.component';
import { InteractionComponent } from '../interaction/interaction.component';


const homeTemplateRoutes: Routes = [
  {
    path: "",
    component: HomeTemplateComponent,
    children: [
      { path: "about", component: AboutComponent },
      { path: "contact", component: ContactComponent },
      { path: "detail", component: DetailComponent },
      { path: "bt1", component: Baitap1Component },
      { path: "bt2", component: Baitap2Component },
      { path: "databind", component: DataBindingComponent },
      { path: "attdir", component: AttributeDirectivesComponent },
      { path: "datghe", component: BaiTapDatGheComponent },
      { path: "interact", component: InteractionComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(homeTemplateRoutes)],
  exports: [RouterModule],
})
export class HomeTemplateRoutingModule {}
