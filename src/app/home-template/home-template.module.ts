import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeTemplateComponent } from "./home-template.component";
import { HeaderComponent } from "./header/header.component";
import { AboutComponent } from "./about/about.component";
import { DetailComponent } from "./detail/detail.component";
import { ContactComponent } from "./contact/contact.component";
import { HomeTemplateRoutingModule } from "./home-template-routing.module";

@NgModule({
  declarations: [
    HomeTemplateComponent,
    HeaderComponent,
    AboutComponent,
    DetailComponent,
    ContactComponent,
  ],
  imports: [CommonModule, HomeTemplateRoutingModule],
  // exports: [
  //   HomeTemplateComponent,
  //   HeaderComponent,
  //   AboutComponent,
  //   DetailComponent,
  //   ContactComponent,
  // ],
})
export class HomeTemplateModule {}
