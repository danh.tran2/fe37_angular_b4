import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BaiTapDatGheComponent } from "./bai-tap-dat-ghe.component";
import { DanhSachGheComponent } from './danh-sach-ghe/danh-sach-ghe.component';
import { GheComponent } from './ghe/ghe.component';

@NgModule({
  declarations: [BaiTapDatGheComponent, DanhSachGheComponent, GheComponent],
  imports: [CommonModule],
  exports: [BaiTapDatGheComponent],
})
export class BaiTapDatGheModule {}
