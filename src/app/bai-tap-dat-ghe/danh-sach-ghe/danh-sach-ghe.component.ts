import { Component, OnInit, ViewChildren, QueryList } from "@angular/core";
import { GheComponent } from "../ghe/ghe.component";
import { Ghe } from "src/app/models/ghe";
import { DanhSachGheService } from "../../services/danh-sach-ghe.service";

@Component({
  selector: "app-danh-sach-ghe",
  templateUrl: "./danh-sach-ghe.component.html",
  styleUrls: ["./danh-sach-ghe.component.scss"],
})
export class DanhSachGheComponent implements OnInit {
  mangGhe: Ghe[] = [];

  mangGheDangDat: any[] = [];

  tongTien: number = 0;

  constructor(private dsGheService: DanhSachGheService) {}

  ngOnInit(): void {
    // Tương ứng với componentDidMount bên React
    this.mangGhe = this.dsGheService.getDanhSachGhe();
  }

  datGhe(gheDangDat: any) {
    if (gheDangDat.DangDat) {
      this.mangGheDangDat.push(gheDangDat);
    } else {
      // Cach 1
      // const index = this.mangGheDangDat.findIndex(
      //   (ghe) => ghe.SoGhe === gheDangDat.SoGhe
      // );
      // if (index !== -1) {
      //   this.mangGheDangDat.splice(index, 1);
      // }

      // Cach 2
      this.mangGheDangDat = this.mangGheDangDat.filter(
        (ghe) => ghe.SoGhe !== gheDangDat.SoGhe
      );
    }
    console.log(this.mangGheDangDat);
  }

  @ViewChildren(GheComponent) tagGheComponent: QueryList<GheComponent>;

  huyGhe(soGhe: any) {
    this.mangGheDangDat = this.mangGheDangDat.filter(
      (ghe) => ghe.SoGhe !== soGhe
    );

    // DOM đến các thẻ ghế trong danh sách ghế => Tìm cái ghế đang muốn huỷ bằng SoGhe => set trạng thai đang đặt thành false
    this.tagGheComponent.forEach((item: GheComponent) => {
      if (item.ghe.SoGhe === soGhe) {
        item.dangDat = false;
      }
    });
  }

  tinhTongTien() {
    return this.mangGheDangDat.reduce((tongTien, gheDangDat) => {
      tongTien += gheDangDat.Gia;
      return tongTien;
    }, 0);
  }
}
