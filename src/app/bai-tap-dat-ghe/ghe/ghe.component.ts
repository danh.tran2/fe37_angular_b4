import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-ghe",
  templateUrl: "./ghe.component.html",
  styleUrls: ["./ghe.component.scss"],
})
export class GheComponent implements OnInit {
  @Input() ghe: any;
  @Output() eventDatGhe = new EventEmitter();
  dangDat: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  datGhe() {
    this.dangDat = !this.dangDat;

    const gheDangDat = {
      SoGhe: this.ghe.SoGhe,
      Gia: this.ghe.Gia,
      DangDat: this.dangDat,
    };

    this.eventDatGhe.emit(gheDangDat);
  }
}
