import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminTemplateComponent } from "./admin-template.component";
import { UserManagementComponent } from "./user-management/user-management.component";
import { MovieManagementComponent } from "./movie-management/movie-management.component";
import { AdminTemplateRoutingModule } from "./admin-template-routing.module";

@NgModule({
  declarations: [
    AdminTemplateComponent,
    UserManagementComponent,
    MovieManagementComponent,
  ],
  imports: [CommonModule, AdminTemplateRoutingModule],
})
export class AdminTemplateModule {}
