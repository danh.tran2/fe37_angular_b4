import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminTemplateComponent } from "./admin-template.component";
import { UserManagementComponent } from "./user-management/user-management.component";
import { MovieManagementComponent } from "./movie-management/movie-management.component";

const adminRoutes: Routes = [
  {
    path: "",
    component: AdminTemplateComponent,
    children: [
      { path: "user", component: UserManagementComponent },
      { path: "movie", component: MovieManagementComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule],
})
export class AdminTemplateRoutingModule {}
