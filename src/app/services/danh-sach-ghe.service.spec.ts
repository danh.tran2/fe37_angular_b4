import { TestBed } from '@angular/core/testing';

import { DanhSachGheService } from './danh-sach-ghe.service';

describe('DanhSachGheService', () => {
  let service: DanhSachGheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DanhSachGheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
