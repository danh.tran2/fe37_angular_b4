import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// import { AboutComponent } from "./home-template/about/about.component";
// import { ContactComponent } from "./home-template/contact/contact.component";
// import { DetailComponent } from "./home-template/detail/detail.component";
import { HomeTemplateModule } from "./home-template/home-template.module";
import { AdminTemplateModule } from "./admin-template/admin-template.module";

// Cách 1: router theo component
// const appRoutes: Routes = [
//   { path: "about", component: AboutComponent },
//   { path: "contact", component: ContactComponent },
//   { path: "detail", component: DetailComponent },
// ];

// Cách 2: router theo modules
const appRoutes: Routes = [
  { path: "", loadChildren: () => HomeTemplateModule },
  { path: "admin", loadChildren: () => AdminTemplateModule },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
